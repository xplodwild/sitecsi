<!--
/*
 * @author root@localhost
 * @creation 2013-04-17 23:54
 * @goal Voler l'argent des gens. Et avoir une bonne note au projet au passage.
 */
-->
<?php include('header.inc.php'); ?>

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Louez pour pas cher!</h1>
        <p>Voitures vol�es, braqu�es et mises en location pour le plaisir de votre porte-monnaie. Certifi� avec fausse carte grise pour �viter tout probl�me lors des contr�les de police.</p>
        <p><a href="#" class="btn btn-primary btn-large" onclick="alert('...Y\'a rien � dire...');">Dites m'en plus &raquo;</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4">
          <h2>Louer</h2>
          <p>Vous voulez louer une voiture ? Dites-nous ce dont vous avez besoin et on ira vous voler �a dans les temps.</p>
          <p><a class="btn" href="louer.php">C'est parti &raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Rendre</h2>
          <p>Vous avez vol� une de nos voitures vol�es et vous voulez nous la rendre ?</p>
          <p><a class="btn" href="rendre.php">Retour voiture &raquo;</a></p>
       </div>
        <div class="span4">
          <h2>Abonnement</h2>
          <p>Vous faites confiance au charlatans que nous sommes ? Checkez votre abonnement !</p>
          <p><a class="btn" href="abo.php">Mon abonnement &raquo;</a></p>
        </div>
      </div>

<?php include('footer.inc.php'); ?>
