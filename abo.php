<?php /* Sentez-vous la qualit� de ce code ? */ ?>
<?php $PAGE="abo"; ?>
<?php include('header.inc.php'); ?>

<h2>Mon abonnement</h2>
<?php if (get_abo_type() == "ab_break") { ?>
<p>Abonnement actif : <b>paquebot</b></p>
<?php $user_id = 1; ?>
<?php } else if (get_abo_type() == "ab_berline") { ?>
<p>Abonnement actif : <b>berline</b></p>
<?php $user_id = 2; ?>
<?php } else if (get_abo_type() == "ab_mature") { ?>
<?php $user_id = 3; ?>
<?php } else { ?>
<p>Non abonn�. Veuillez nous faire un ch�que.</p>
<?php } ?>
<hr>

<?php if (get_abo_type() != "non") { ?>
<h4>R�sum� de votre abonnement</h4>
<hr>
<div class="span5">
<?php $query = mysql_query("SELECT * FROM client WHERE idClient='".intval($user_id)."' LIMIT 1");
$data = mysql_fetch_array($query); ?>
<b>Nom : </b>M. <?php echo $data['Nom']; ?> <?php echo $data['Prenom']; ?><br />
<b>Adresse : </b><?php echo $data['Adresse']; ?><br />
<b>E-mail : </b><?php echo $data['Mail']; ?><br />
<b>T�l�phone : </b><?php echo $data['Telephone']; ?><br />
<b>RIB : </b><?php echo $data['RIB']; ?>
</div>
<div class="span5 pull-right">
<b>Num. Permis de Conduire : </b><?php echo $data['NumPermisConduire']; ?><br />
<b>Date Permis de Conduire : </b><?php echo $data['DatePermisConduire']; ?><br />
<b>Pr�fecture : </b><?php echo $data['PrefecturePermisConduire']; ?><br />
<?php if ($user_id == 2) { ?><b>Photo permis de conduire : </b><img src="img/cadeau.png" style="width:100px" /><br /><i><a href="javascript:alert('Nous nous engageons a respecter votre vie priv�e et � ne pas divulger vos informations � tierce partie, en dehors des personnes accr�dit�es � consulter ce projet.');">Confidentialit�</a></i><?php } ?>
</div>

<div class="clearfix"></div>
<br><br>
<h4>Etat de l'abonnement</h4>
<hr>
<table class="table table-striped">
<thead>
<tr>
<th>Tarif</th>
<th>R�duction</th>
<th>Date d'abonnement</th>
<th>Date d'expiration</th>
</tr>
</thead>

<tbody>
<?php
$query_abos = mysql_query("SELECT * FROM abonnement WHERE idClient='".intval($user_id)."' ORDER BY DateFin ASC");
$datelaplusrecente = 0;

while ($abo = mysql_fetch_array($query_abos)) { ?>
<tr>
<td><?php echo number_format($abo['Tarif'], 2); ?> &euro;</td>
<td><?php echo number_format($abo['Reduction'], 2); ?> &euro;</td>
<td><?php echo date('d/m/Y, H:i', strtotime($abo['DateDebut'])); ?></td>
<td><?php echo date('d/m/Y, H:i', strtotime($abo['DateFin'])); ?></td>
<?php if (strtotime($abo['DateFin']) > $datelaplusrecente) { $datelaplusrecente = strtotime($abo['DateFin']); } ?>
</tr>
<?php
}
?>
</tbody>
</table>

<?php if (time() < $datelaplusrecente) {?>
<div class="alert alert-success">Votre abonnement est actif jusqu'au <?php echo date('d/m/Y � H:i', $datelaplusrecente); ?></div>
<?php } else { ?>
<div class="modal hide fade" id="modalAbo" role="dialog" aria-labelledby="modalAbo" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Renouveler son abonnement</h3>
	</div>
	<div class="modal-body">
		<p>Plus la dur�e de pigeonnage est longue, moins elle est ch�re !</p>
		<b>Dur�e :</b>
		<select id="dd-duree" onchange="changeabo();">
			<option value="3">3 mois</option>
			<option value="6">6 mois (5% r�duction)</option>
			<option value="9">9 mois (10% r�duction)</option>
			<option value="12">12 mois (15% r�duction)</option>
		</select></p>
		<p><b>Prix :</b> <span id="prix">4011</span>&euro;</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Annuler</a>
		<a href="#" class="btn btn-success" onclick="renewabo();return false">Renouveler</a>
	</div>
</div>
<a class="btn btn-danger" href="#modalAbo" role="button" data-toggle="modal">Votre abonnement a expir� le <?php echo date('d/m/Y � H:i', $datelaplusrecente); ?>. Veuillez le renouveler.</a>
<?php }?>

<?php } ?>

<script type="text/javascript">
function renewabo() {
	$.get("truc.qui.recoit.php", {q:'renew', p:$("#prix").text() /* securite+++++++ */, d:$("#dd-duree").val(), r:1337*$("#dd-duree").val()-$("#prix").text()}, function() {
		window.location.reload();
	});
}

function changeabo() {
	var mois = $("#dd-duree").val();
	var base = 1337 * mois;
	if (mois == 6)
		base = base*0.95;
	else if (mois == 9)
		base = base*0.9;
	else if (mois == 12)
		base = base*0.85;

	$("#prix").fadeOut(500, function() {
		$("#prix").text(base.toFixed(2));
		$("#prix").fadeIn(500);
	});
}
</script>

<?php
include('footer.inc.php');
?>
